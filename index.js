console.log(`Soal 1`);
console.log(`-----------------------------------------`);
let s = "";
let pola = 10;
for (let i = 1; i <= pola; i++) {
  for (let j = pola; j >= i; j--) {
    s += " ";
  }
  for (let k = 1; k <= i + (i - 1); k++) {
    s += "x";
  }

  s += `\n`;
}
for (let i = 1; i <= pola; i++) {
  for (let j = 1; j <= i; j++) {
    s += " ";
  }

  for (let k = pola; k >= i + (i - pola); k--) {
    s += "x";
  }

  s += `\n`;
}

console.log(`----Jawaban No 1`, s);

console.log(`-----------------------------------------`);
console.log(`Soal 2`);
console.log(`-----------------------------------------`);

let leaderboards = [200, 100, 100, 50, 10, 100];
let player = [20, 80, 80, 100, 100, 200];

leaderboards = Array.from(new Set(leaderboards));
const player_ranks = [];
for (const score of player) {
  while (leaderboards && score >= leaderboards[leaderboards.length - 1])
    leaderboards.pop();
  player_ranks.push(leaderboards.length + 1);
}

console.log(`----Jawaban No 2 : `, player_ranks.toString());

console.log(`-----------------------------------------`);
console.log(`Soal 3`);
console.log(`-----------------------------------------`);

let a = 17;
let b = 8;

let hasil = "";
for (let i = 0; i < a; i++) {
  if (i % 3 == 0) {
    for (let i = 1; i < b; i++) {
      hasil += "b";
    }
  }
  hasil += "a";
}

console.log(`----Jawaban No 3 : `, hasil);
